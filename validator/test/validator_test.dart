import 'package:flutter_test/flutter_test.dart';

import 'package:validator/validator.dart';

void main() {
  test('test string to number convertion', () {
    expect(CommonValidation().isNumeric("1234F"), false);
    expect(CommonValidation().isNumeric("12347"), true);
  });
  test('test pincode input', () {
    PincodeValidator("123456", isLengthValidation: true)
        .validate()
        .catchError((onError) {
      expect(onError, 'Pincode length not match');
    });
    PincodeValidator("10048").validate().catchError((onError) {
      expect(onError, 'Same digit exit twice next to each other');
    });
    PincodeValidator("00000").validate().catchError((onError) {
      expect(onError, 'Same digit exit twice next to each other');
    });
    PincodeValidator("12345").validate().catchError((onError) {
      expect(onError, 'third (+) number may be in sequence [1 ,2 ,3]');
    });
    PincodeValidator("98765").validate().catchError((onError) {
      expect(onError, 'third (-) number may be in sequence [9 ,8 ,7]');
    });

    PincodeValidator("12548").validate().then((onValue) {
      expect(onValue, 'Pincode is valid!');
    });
  });
}
