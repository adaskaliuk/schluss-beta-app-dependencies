/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-05-08 18:05:01
 * @modify date 2020-05-08 18:05:01
 */

import 'dart:core';
import 'package:logger/logger.dart';
import './common_validation.dart';

/* 
Rules:
Length of 5 digits: A pincode must always be exact 5 digits long.
No repeating: a digit can exist twice in a pincode but not next to each other:
OK: 10480 Not OK: 10048 / 00000
No sequences: only two numbers may be in sequence.
OK: 12548 Not OK: 12345 / 12300 / 98765 
*/
class PincodeValidator {
  var logger =
      Logger(); //create instence of Small, easy to use and extensible logger which prints beautiful logs.

  final int pincodeLength;
  final bool isLengthValidation;
  final bool isRepeatedDigitsValidation;
  final bool isNumberSequenceValidation;
  final String pincode;

  PincodeValidator(
    this.pincode, {
    this.pincodeLength = 5,
    this.isLengthValidation = true,
    this.isNumberSequenceValidation = true,
    this.isRepeatedDigitsValidation = true,
  });

  Future<String> validate() async {
    if (!CommonValidation()
        .isNumeric(pincode)) // check pincode string is numberic
      return Future.error('Pincode contains invalid charactors');
    if (isLengthValidation && !_validateLength()) {
      return Future.error('Pincode length not match');
    }
    List<String> list = pincode.split(''); // extract digit from pincode string
    for (int i = 0; i < list.length - 1; i++) {
      int currentDigit = int.parse(list[i]);
      int nextDigit = int.parse(list[i + 1]); // next digit
      if (isRepeatedDigitsValidation &&
          !_validateDuplicatedDigits(currentDigit, nextDigit)) {
        //check consecutive repeated
        return Future.error("Same digit exit twice next to each other");
      }
      if (isNumberSequenceValidation && i <= list.length - 3) {
        // check incremented number sequence in pincode
        int thirdDigit =
            int.parse(list[i + 2]); //get third consecutive digit from pincode
        if (!_validateIncrementedSequence(
            currentDigit, nextDigit, thirdDigit)) {
          return Future.error(
              "third (+) number may be in sequence [$currentDigit ,$nextDigit ,$thirdDigit]");
        }
        if (!_validateDecrementedSequence(
            // check decremented number sequence in pincode
            currentDigit,
            nextDigit,
            thirdDigit)) {
          return Future.error(
              "third (-) number may be in sequence [$currentDigit ,$nextDigit ,$thirdDigit]");
        }
      }
    }
    logger.i("Pincode validation done!");
    return Future.value('Pincode is valid!'); // pincode is valid
  }

  // validate pincode length
  bool _validateLength() {
    if (pincode.trim().length != pincodeLength) {
      logger.e('Pincode length not match, expected pincode length :' +
          pincodeLength.toString());
      return false;
    }
    return true;
  }

  /*
    * Validate repeated digits
    * A digit can exist twice in a pincode but not next to each other 
   */
  bool _validateDuplicatedDigits(currentDigit, nextDigit) {
    if (currentDigit == nextDigit) {
      logger.e('Same digit exit twice next to each other');
      return false;
    }
    return true;
  }

  /*
    * check incremented[+] sequence in digits
    * Not allowed: 12345 
   */
  bool _validateIncrementedSequence(currentDigit, nextDigit, thirdDigit) {
    if (currentDigit + 1 == nextDigit && currentDigit + 2 == thirdDigit) {
      logger.e(
          "third (+) number may be in sequence [$currentDigit ,$nextDigit ,$thirdDigit]");
      return false;
    }
    return true;
  }

/*
    * check incremented[+] sequence in digits
    * Not allowed: 54321 
   */
  bool _validateDecrementedSequence(currentDigit, nextDigit, thirdDigit) {
    if (currentDigit - 1 == nextDigit && currentDigit - 2 == thirdDigit) {
      logger.e(
          "third (-) number may be in sequence [$currentDigit ,$nextDigit ,$thirdDigit]");
      return false;
    }
    return true;
  }

  @override
  String toString() {
    return 'PincodeValidator(pincodeLength: $pincodeLength, isLengthValidation: $isLengthValidation, isRepeatedDigitsValidation: $isRepeatedDigitsValidation, isNumberSequenceValidation: $isNumberSequenceValidation, pincode: $pincode)';
  }
}
