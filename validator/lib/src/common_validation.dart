/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-05-09 23:41:00
 * @modify date 2020-05-09 23:41:00
 */
//Add common schluss validations 
class CommonValidation {
  
  //check string contains only numbers
  bool isNumeric(String str) {
    if (str == null) {
      return false;
    }
    return double.tryParse(str) != null;
  }
}
